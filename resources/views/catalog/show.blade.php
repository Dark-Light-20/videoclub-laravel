@extends('layouts.master')

@section('content')
<div class="row">
    <div class="col-sm-4">
        <img class="img-fluid" src="{{$pelicula->poster}}"/>
    </div>
    <div class="col-sm-8">
        <h2>{{ $pelicula->title }}</h2>
        <h4>Año: {{ $pelicula->year }}</h4>
        <h4>Director: {{ $pelicula->director }}</h4>
        <p><b>Resumen:</b> {{ $pelicula->synopsis }}</p>
        <p>
            <b>Estado:</b>
            @if ($pelicula->rented)
            Pelicula actualmente alquilada.
            @else
            Pelicula actualmente disponible.
            @endif
        </p>
        @if (!$pelicula->rented)
        <button type="button" class="btn btn-primary">Alquilar</button>
        @else
        <button type="button" class="btn btn-danger">Devolver pelicula</button>
        @endif
        <a href="{{ url('/catalog/edit/' . $pelicula->id ) }}" type="button" class="btn btn-warning"> <i class="fa fa-pencil"></i> Editar pelicula</button>
        <a href="{{ url('/catalog') }}" class="btn btn-light"> <i class="fa fa-chevron-left"></i> Volver al listado</a>
    </div>
</div>
@stop