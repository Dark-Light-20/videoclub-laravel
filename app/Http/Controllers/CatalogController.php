<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Movie;

class CatalogController extends Controller
{
    public function getIndex()
    {
        return view('catalog.index', array('arrayPeliculas' => Movie::all()));
    }

    public function getShow($id)
    {
        return view('catalog.show', array('pelicula' => Movie::findOrFail($id)));
    }

    public function getCreate()
    {
        return view('catalog.create');
    }

    public function getEdit($id)
    {
        return view('catalog.edit', array('pelicula' => Movie::findOrFail($id)));
    }

    public function postCreate(Request $request)
    {
        $newMovie = new Movie();
        $newMovie->title = $request->title;
        $newMovie->year = $request->year;
        $newMovie->director = $request->director;
        $newMovie->poster = $request->poster;
        $newMovie->synopsis = $request->synopsis;
        $newMovie->save();
        return redirect('/catalog');
    }

    public function putEdit(Request $request, $id)
    {
        $movie = Movie::findOrFail($id);
        $movie->title = $request->title;
        $movie->year = $request->year;
        $movie->director = $request->director;
        $movie->poster = $request->poster;
        $movie->synopsis = $request->synopsis;
        $movie->save();
        return redirect('/catalog/show/'.$id);
    }
}
